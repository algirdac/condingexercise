﻿using System;

namespace CodingExerciseApp
{
    public class ProductItem : IRequirement
    {
        public ProductItem(int id, String name, bool adultReq, int incomeReq, bool studentReq, int productType)
        {
            Id = id;
            ProductName = name;
            IsAdultRequirement = adultReq;
            MinIncomeRequirement = incomeReq;
            IsStudentRequirement = studentReq;
            ProductType = productType;
        }
        public int Id { get; set; }
        public String ProductName { get; set; }
        public bool IsAdultRequirement { get; set; }
        public int MinIncomeRequirement { get; set; }
        public bool IsStudentRequirement { get; set; }
        public int ProductType { get; set; }
    }
}
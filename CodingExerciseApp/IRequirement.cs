﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingExerciseApp
{
   public interface IRequirement
   {
         bool IsAdultRequirement { get; set; }

         int MinIncomeRequirement { get; set; }

         bool IsStudentRequirement { get; set; }
    }
}

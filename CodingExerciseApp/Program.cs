﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingExerciseApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var random = new Random();
            int age = random.Next(1, 99);
            bool isStudent = random.Next(2) == 0;
            int income = random.Next(0, 50000);

            Console.WriteLine("User input: Age = " + age + ", Student = " + isStudent + ", Income = " + income +"\n");
            var bundle = DataService.GetRecommendedBundle(age, isStudent, income);
            DataService.PrintBundle(bundle);
            if(bundle != null)
            {
                var products = DataService.GetAvailableProductsFroModification(age, isStudent, income, bundle);
                DataService.PrintProducts(products);
            }
             
            Console.ReadLine();
        }

    }
}

﻿using System;
using System.Collections.Generic;

namespace CodingExerciseApp
{
    public class BundleItem: IRequirement
    {
        public BundleItem(int id, String bundleName, bool adultReq, int incomeReq, bool studentReq, int value, List<ProductItem> products)
        {
            Id = id;
            BundleName = bundleName;
            Value = value;
            IsAdultRequirement = adultReq;
            MinIncomeRequirement = incomeReq;
            IsStudentRequirement = studentReq;
            ProductsIncluded = products;
        }

        public int Id { get; set; }
        public String BundleName { get; set; }
        public List<ProductItem> ProductsIncluded { get; set; }
        public int Value { get; set; }
        public bool IsAdultRequirement { get; set; }
        public int MinIncomeRequirement {get; set;}
        public bool IsStudentRequirement { get; set; }
    }
}
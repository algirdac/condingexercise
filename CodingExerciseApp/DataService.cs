﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CodingExerciseApp
{
    public static class DataService
    {
        private static List<BundleItem> BundleList;

        private static List<ProductItem> ProductList;

        private static List<BundleItem> GetBundleList()
        {
            if(BundleList == null)
            {
                var currentAccount = new ProductItem(1, "Current Account", true, 1, false, 0);
                var currentAccountPlus = new ProductItem(2, "Current Account Plus", true, 40000, false, 0);
                var juniorSaverAccount = new ProductItem(3, "Junior Saver Account", false, 0, false, 0);
                var studentAccount = new ProductItem(4, "Student Account", true, 0, true, 0);
                var debitCard = new ProductItem(5, "Debit Card", true, 0, false, 1);
                var creditCard = new ProductItem(6, "Credit Card", true, 12000, false, 2);
                var goldCreditCard =  new ProductItem(7, "Gold Credit Card", true, 40000, false, 2);
                ProductList = new List<ProductItem> { currentAccount, currentAccountPlus, juniorSaverAccount, studentAccount, debitCard, creditCard, goldCreditCard };
                BundleList = new List<BundleItem>
                {
                    //Junior          Saver Junior Saver;                                     Age < 18                   0
                    //Student         Student Account, Debit Card, Credit Card                Age > 17 & Student = Yes   0
                    //Classic         Current Account, Debit Card                             Age > 17 & Income > 0      1
                    //Classic Plus    Current Account, Debit Card, Credit Card                Income > 12000 & Age > 17  2
                    //Gold            Current Account Plus, Debit Card, Gold Credit Card      Income > 40000 & Age > 17  3
                    new BundleItem(1, "Junior Saver",  false, 0,       false, 0, new List<ProductItem> { juniorSaverAccount }),
                    new BundleItem(2, "Student",       true,  0,       true,  0, new List<ProductItem> { studentAccount, debitCard, creditCard}),
                    new BundleItem(3, "Classic",       true,  1,       false, 1, new List<ProductItem> { currentAccount, debitCard, }) ,
                    new BundleItem(4, "Classic Plus ", true,  12000,   false, 2, new List<ProductItem> { currentAccount, debitCard, creditCard}),
                    new BundleItem(5, "Gold",          true,  40000,   false, 3, new List<ProductItem> { currentAccountPlus, debitCard, goldCreditCard}),
                };
            }
            return BundleList;
        }

        public static List<ProductItem> GetProductList()
        {
            if (ProductList == null)
            {
                ProductList = new List<ProductItem>
                {
                    //Current Account Income > 0 & Age > 17
                    //Current Account Plus Income > 40000 & Age > 17
                    //Junior Saver Account Age < 18
                    //Student Account Student = Yes & Age > 17
                    //Debit Card Bundle must include one of: Current Account, Current Account Plus, Student Account, or Pensioner Account
                    //Credit Card Income > 12000 & Age > 17
                    //Gold Credit Card Income > 40000 & Age > 17

                    new ProductItem(1, "Current Account",      true,  1,      false, 0),
                    new ProductItem(2, "Current Account Plus", true,  40000,  false, 0),
                    new ProductItem(3, "Junior Saver Account", false, 0,      false, 0),
                    new ProductItem(4, "Student Account",      true,  0,      true,  0),
                    new ProductItem(5, "Debit Card",           true,  0,      false, 1),
                    new ProductItem(6, "Credit Card",          true,  12000,  false, 2),
                    new ProductItem(7, "Gold Credit Card",     true,  40000,  false, 2),
                };
            }
            return ProductList;
        }

        private static IEnumerable<IRequirement> FilterListByRequirements(IEnumerable<IRequirement> list, int age, bool isStudent, int income)
        {
            var isAdult = age > 17;
            var lst = list.Where(bundle => bundle.IsAdultRequirement == isAdult);
            if (isAdult)
            {
                if (isStudent)
                {
                    lst = lst.Where(r => r.IsStudentRequirement == isStudent || income >= r.MinIncomeRequirement);
                }
                else
                {
                    lst = lst.Where(r => r.IsStudentRequirement == isStudent && income >= r.MinIncomeRequirement);
                }
            }
            return lst;
        }

        public static BundleItem GetRecommendedBundle(int age, bool isStudent, int income)
        {
            var availableBundles = FilterListByRequirements(GetBundleList(), age, isStudent, income);
            return availableBundles.OfType<BundleItem>().OrderByDescending(o => o.Value).FirstOrDefault();         
        }

        public static List<ProductItem> GetAvailableProductsFroModification(int age, bool isStudent, int income, BundleItem bundleItem)
        {
            var products = FilterListByRequirements(GetProductList(), age, isStudent, income);
            return products.OfType<ProductItem>().Where(p => !bundleItem.ProductsIncluded.Contains(p)).ToList();
        }


        public static void PrintBundle(BundleItem bundle)
        {
            if (bundle == null)
            {
                Console.WriteLine("No recommended bundle");
            }
            else
            {
                Console.WriteLine("Recomented bundle name: " + bundle.BundleName);
                foreach (var p in bundle.ProductsIncluded)
                {
                    Console.WriteLine("-Product: " + p.Id + " " + p.ProductName);
                }
            }
            Console.WriteLine();
        }

        public static void PrintProducts(IEnumerable<ProductItem> list)
        {
            if (list == null)
            {
                Console.WriteLine("No products available for modification");
            }
            else
            {
                Console.WriteLine("Products available for modification:");
                foreach (var p in list)
                {
                    Console.WriteLine("-Product: " + p.Id + " " + p.ProductName);
                }
            }
            Console.WriteLine();
        }

        public static bool ConfirmBundleModifications(BundleItem bundleItem, out String message)
        {
            string confirmationMessage = "";
            bool isConfirmed = true;
            int debidCardType = 1;
            int accountType = 0;
            if(bundleItem != null)
            {
                if (bundleItem.ProductsIncluded.Where(p => p.ProductType == 0).Count() > 1)
                {
                    confirmationMessage += "A customer may only have one account \n";
                    isConfirmed = false;
                } 
                
                if(bundleItem.ProductsIncluded.Any(p=>p.ProductType == debidCardType) && !bundleItem.ProductsIncluded.Any(p => p.ProductType == accountType))
                {
                    confirmationMessage += "Bundle must include one of... \n";
                    isConfirmed = false;
                }
            }
            message = confirmationMessage;
            return isConfirmed;
        }
    }
}
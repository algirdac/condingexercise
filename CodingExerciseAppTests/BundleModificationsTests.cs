﻿using System;
using CodingExerciseApp;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace CodingExerciseAppTests
{
    [TestClass]
    public class BundleModificationsTests
    {
        [TestMethod]
        public void TestBundleModifications_AccountCase()
        {
            var bundle = DataService.GetRecommendedBundle(50, false, 40000);
            Assert.IsNotNull(bundle);
            var curentAccountProduct = DataService.GetProductList().FirstOrDefault(p => p.Id == 1);
            bundle.ProductsIncluded.Add(curentAccountProduct);

            string errMessage = "";
            var isConfirmed = DataService.ConfirmBundleModifications(bundle, out errMessage);
            Console.WriteLine(errMessage);
            Assert.AreEqual(false, isConfirmed);
        }

        [TestMethod]
        public void TestBundleModifications_DebitCardCase()
        {
            var bundle = DataService.GetRecommendedBundle(50, false, 12001);
            Assert.IsNotNull(bundle);
            var curentAccountProduct = DataService.GetProductList().FirstOrDefault(p => p.Id == 1);
            bundle.ProductsIncluded.Remove(curentAccountProduct);

            string errMessage = "";
            var isConfirmed = DataService.ConfirmBundleModifications(bundle, out errMessage);
            Console.WriteLine(errMessage);
            Assert.AreEqual(false, isConfirmed);
        }
    }
}

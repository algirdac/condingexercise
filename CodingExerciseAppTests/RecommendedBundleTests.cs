﻿using System;
using CodingExerciseApp;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CodingExerciseAppTests
{
    [TestClass]
    public class RecommendedBundleTests
    {
        [TestMethod]
        public void TestGetRecommendedBundleCase1()
        {
            var bundle = DataService.GetRecommendedBundle(17, false, 0);
            Assert.IsNotNull(bundle);
            Assert.AreEqual(1, bundle.Id);

        }
        [TestMethod]
        public void TestGetRecommendedBundleCase2()
        {

            var bundle = DataService.GetRecommendedBundle(17, true, 50000);
            Assert.IsNotNull(bundle);
            Assert.AreEqual(1, bundle.Id);
        }

        [TestMethod]
        public void TestGetRecommendedBundleCase3()
        {

            var bundle = DataService.GetRecommendedBundle(25, true, 50000);
            Assert.IsNotNull(bundle);
            Assert.AreEqual(5, bundle.Id);
        }

        [TestMethod]
        public void TestGetRecommendedBundleCase4()
        {

            var bundle = DataService.GetRecommendedBundle(50, false, 0);
            Assert.IsNull(bundle);
        }

        [TestMethod]
        public void TestGetRecommendedBundleCase5()
        {
            var bundle = DataService.GetRecommendedBundle(50, false, 1);
            Assert.IsNotNull(bundle);
            Assert.AreEqual(3, bundle.Id);
        }

    }
}
